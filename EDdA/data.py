from hashlib import md5
import pandas
from os.path import isfile

class Source:
    def __init__(self, articles):
        self.articles = articles
        articleIDs = articles.volume.map(str) + '-' + articles.numero.map(str)
        self.hash = md5(':'.join(articleIDs).encode('ascii')).hexdigest()

def load(name, textColumn="contentWithoutClass",
        classColumn="ensemble_domaine_enccre"):
    fileName = name if isfile(name) else "datasets/{name}.tsv".format(name=name)
    return Source(pandas.read_csv(fileName, sep='\t', na_filter=False)\
            .dropna(subset=[classColumn])\
            .reset_index(drop=True)
        )

domains = [
           'Agriculture - Economie rustique', 'Anatomie', 'Antiquité',
           'Architecture', 'Arts et métiers', 'Beaux-arts',
           'Belles-lettres - Poésie', 'Blason', 'Caractères', 'Chasse',
           'Chimie', 'Commerce', 'Droit - Jurisprudence',
           'Economie domestique', 'Grammaire', 'Géographie', 'Histoire',
           'Histoire naturelle', 'Jeu', 'Marine', 'Maréchage - Manège',
           'Mathématiques', 'Mesure', 'Militaire (Art) - Guerre - Arme',
           'Minéralogie', 'Monnaie', 'Musique', 'Médailles',
           'Médecine - Chirurgie', 'Métiers', 'Pharmacie', 'Philosophie',
           'Physique - [Sciences physico-mathématiques]', 'Politique',
           'Pêche', 'Religion', 'Spectacle', 'Superstition'
        ]

domainId = dict([(domains[k], k) for k in range(0, len(domains))])

def shortDomain(name, maxSize=20):
    if len(name) > maxSize:
        components = name.split(' ')
        return components[0] + ' […]'
    else:
        return name

def domain(articles, name):
    return articles[articles.ensemble_domaine_enccre == name]
