from EDdA import data
import math

def keysIntersection(d1, d2):
    return len(set(d1).intersection(d2))

def scalarProduct(d1, d2):
    return sum([d1[k] * d2[k] for k in set(d1.keys()).intersection(d2)])

def norm(d):
    return math.sqrt(scalarProduct(d, d))

def colinearity(d1, d2):
    return scalarProduct(d1, d2) / (norm(d1) * norm(d2))

metrics = {
        'keysIntersection': keysIntersection,
        'colinearity': colinearity
    }

""" the variable 'domains' allows to restrict the matrices we're computing, but
"   for our current needs they're still supposed to be about the classes devised
"   for GÉODE so we give it this default value
"""
def confusionMatrix(vectorizer, metric, domains=data.domains):
    m = []
    matrixSize = len(domains)
    for a in range(0, matrixSize):
        m.append(matrixSize * [None])
        for b in range(0, matrixSize):
            m[a][b] = metric(vectorizer(domains[a]), vectorizer(domains[b]))
    return m
