from EDdA.classification.nGramsFrequencies import topNGrams
from EDdA.classification.classSimilarities \
        import colinearity, confusionMatrix, keysIntersection, metrics
from EDdA.classification.visualization import heatmap, histogram, showGraph
