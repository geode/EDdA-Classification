from EDdA import data
from EDdA.store import preparePath
import graphviz
from math import ceil
import matplotlib.cm as cm
import matplotlib
import matplotlib.pyplot as plot
import numpy
import seaborn

def colorize(vmin, vmax, cmap):
    mapper = cm.ScalarMappable(
            norm=matplotlib.colors.Normalize(vmin=vmin, vmax=vmax, clip=True),
            cmap=seaborn.color_palette(cmap, as_cmap=True)
        )
    def toHexRGB(color):
        (r, g, b, _) = mapper.to_rgba(color)
        return f"#{int(r*255):02x}{int(g*255):02x}{int(b*255):02x}"
    return toHexRGB

def showGraph(adjacency, filePath, cmap='Blues'):
    edgeValues = [x for row in adjacency for x in row if x is not None]
    color = colorize(min(edgeValues), max(edgeValues), cmap)
    g = graphviz.Digraph()
    g.graph_attr['rankdir'] = 'LR'
    dimension = len(adjacency)
    for i in range(0, dimension):
        g.node(str(i), label=data.shortDomain(data.domains[i]))
    for i in range(0, dimension):
        for j in range(0, len(adjacency[i])):
            link = adjacency[i][j]
            if link is not None:
                label = f"{link}" if type(link) == int else f"{link:.2f}"
                g.edge(str(i), str(j), color=color(link), label=label)
    #return Image(filename=g.render(preparePath(filePath), format='png'))
    return g.render(preparePath(filePath), format='png')

def heatmap(matrix, filePath, domains=list(map(data.shortDomain, data.domains)), **kwargs):
    plot.figure(figsize=(16,13))
    if 'cmap' not in kwargs:
        kwargs['cmap'] = 'Blues'
    ax = seaborn.heatmap(
            matrix, xticklabels=domains, yticklabels=domains, **kwargs
        )
    plot.savefig(filePath, dpi=300, bbox_inches='tight')

def topValue(delta, ticks=5):
    i = 0
    if delta < 3:
        return topValue(10*delta, ticks)/10
    elif delta > 100:
        return topValue(delta/10, ticks)*10
    elif delta > 50:
        return topValue(delta/5, ticks)*5
    else:
        delta = ceil(delta)
        while (delta + i) % ticks > 0:
            i += 1
        return delta + i

def histogram(keys, values, filePath, minValue=0, maxValue=None):
    fig, ax = plot.subplots(figsize = (18, 6))
    maxValue = max(values) if maxValue is None else maxValue
    delta = topValue(maxValue - minValue)
    yTop = minValue + (1.1*delta)
    yTicks = numpy.arange(minValue, yTop, delta / 5)
    ax.hlines(y=yTicks, xmin=-1, xmax=len(keys)-1, color="#bfbfbf", lw=0.6)

    ax.set_xlim(-0.5, len(keys) - 0.5)
    ax.set_ylim(0, yTop)
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['left'].set_visible(False)
    bars = ax.bar(keys, values, width=0.5)
    for bar in bars :
      height = bar.get_height()
      ax.text(bar.get_x() + bar.get_width()/2., height + (delta/30),
                    '%.4g' % height, ha='center', va='bottom', fontsize=12, rotation=90)

    plot.xticks(fontsize=12, rotation=45, ha='right')
    plot.yticks(yTicks, fontsize=12)
    fig.savefig(filePath, bbox_inches = 'tight', dpi=150)
