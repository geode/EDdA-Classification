import pickle

def vectorizerFileName(name, samplingSize):
    return "{name}_s{samplingSize}".format(name=name, samplingSize=samplingSize)

def vectorizer(name, samplingSize=10000):
    filePath = "models/{fileName}.pkl".format(
            fileName=vectorizerFileName(name, samplingSize)
        )
    with open(filePath, 'rb') as file:
      return pickle.load(file)

def classifier(name, vectorizerName, samplingSize=10000):
    filePath = "models/{name}_{vectorizer}.pkl".format(
            name=name,
            vectorizer=vectorizerFileName(vectorizerName, samplingSize)
        )
    with open(filePath, 'rb') as file:
        return pickle.load(file)
