#!/usr/bin/env python3

from setuptools import setup

setup(name='PyEDdA',
      version='0.1',
      packages=['EDdA', 'EDdA.classification'],
      scripts=['scripts/modelConfusionMatrix.py'])
