#!/usr/bin/env python3

from EDdA.store import preparePath
import pandas
from sklearn.metrics import confusion_matrix
from EDdA.classification import heatmap
import sys

def modelConfusionMatrix():
    if len(sys.argv) == 3:
        input_csv = sys.argv[1]
        output_png = sys.argv[2]
        model = pandas.read_csv(input_csv, index_col=0)
        labels, predictions = model['labels'], model['predictions']
        confusion = confusion_matrix(labels, predictions, normalize='true')
        heatmap(confusion, preparePath(output_png))
    else:
        print(f"Syntax: {sys.argv[0]} INPUT_CSV OUTPUT_PNG")

if __name__ == '__main__':
    modelConfusionMatrix()
