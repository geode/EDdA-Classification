(use-modules ((gnu packages machine-learning) #:select (python-pytorch python-scikit-learn))
             ((gnu packages python-science) #:select (python-pandas))
             ((gnu packages python-xyz) #:select (python-matplotlib
                                                  python-nltk
                                                  python-numpy
                                                  python-seaborn))
             ((gnu packages graphviz) #:select (graphviz python-graphviz))
             (guix gexp)
             (guix git-download)
             ((guix licenses) #:select (lgpl3+))
             (guix packages)
             (guix build-system python))

(let
  ((%source-dir (dirname (current-filename))))
  (package
    (name "python-pyedda")
    (version "0.1.0")
    (source
      (local-file %source-dir
                  #:recursive? #t
                  #:select? (git-predicate %source-dir)))
    (build-system python-build-system)
    (propagated-inputs
      (list graphviz
            python-graphviz
            python-matplotlib
            python-nltk
            python-numpy
            python-pandas
            python-pytorch
            python-scikit-learn
            python-seaborn))
    (home-page "https://gitlab.liris.cnrs.fr/geode/pyedda")
    (synopsis "A set of tools to explore the EDdA")
    (description
      "PyEDdA provides a python library to expose the data from the Encyclopédie
      by Diderot & d'Alembert, as well as several subpackages for the various
      approach tested in the course of project GÉODE.")
    (license lgpl3+)))
